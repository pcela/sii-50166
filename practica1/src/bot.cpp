// bot.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include "DatosMemCompartida.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <iostream>
#include <stdlib.h>
#include "DatosMemCompartida.h"

using namespace std;

int main(void) {
  DatosMemCompartida *dmc_bot, dmc;
  int fd_bot = open("/tmp/mi_bot", O_RDWR);
  if(fd_bot < 0) {
   cout << "Error en la apertura del fd_bot." << endl;   
   exit(1);
  }
  dmc_bot = (DatosMemCompartida*)mmap(0, sizeof(dmc), PROT_READ | PROT_WRITE, MAP_SHARED, fd_bot, 0);
  if(dmc_bot < 0) {
   cout << "Error en la apertura del fd_bot." << endl;   
   exit(1);
  }
  
  close (fd_bot);
  
  while(true) {
    float distancia = (dmc_bot->raqueta1.y2 + dmc_bot->raqueta1.y1)/2;
    
    if((dmc_bot->esfera).centro.y > distancia)
      dmc_bot->accion = 1;  //P'arriba
    
    if((dmc_bot->esfera).centro.y < distancia)
      dmc_bot->accion = -1; //P'abajo
      
    if((dmc_bot->esfera).centro.y == distancia)
      dmc_bot->accion = 0;  //No hase nah
    
    usleep(25000);
    if(dmc_bot->salir) break;
  }
  return 0;
  
}

