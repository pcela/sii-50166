// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

//#include<errno.h>
#include <stdlib.h>
#include <cstdlib>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <iostream>
#include <unistd.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
   close(fd_Cli2Ser);
   unlink("/tmp/fifo_cli2ser");
   
   close(fd_Ser2Cli);
   unlink("/tmp/fifo_ser2cli");
   
   p_dmc->salir = true;

}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();
   if(disparo1 != NULL) disparo1->Dibuja();
   if(disparo2 != NULL) disparo2->Dibuja();
	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
// 	jugador1.Mueve(0.025f);
// 	jugador2.Mueve(0.025f);
// 	esfera.Mueve(0.025f);
// 	esfera.Decrece(0.025f);
//    	if(disparo1 != NULL) disparo1->Mueve(0.025f);
   
// 	int i;
// 	for(i=0;i<paredes.size();i++)
// 	{
// 		paredes[i].Rebota(esfera);
// 		paredes[i].Rebota(jugador1);
// 		paredes[i].Rebota(jugador2);
//       paredes[i].Colisiona(disparo1);
//       paredes[i].Colisiona(disparo2);
// 	}
// 
// 	jugador1.Rebota(esfera);
// 	jugador2.Rebota(esfera);
//    jugador2.Colisiona(disparo1);
//    jugador1.Colisiona(disparo2);
//    
// 	if(fondo_izq.Rebota(esfera))
// 	{
// 		esfera.centro.x=0;
// 		esfera.centro.y=rand()/(float)RAND_MAX;
// 		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
// 		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
// 		puntos2++;
// 	}
// 
// 	if(fondo_dcho.Rebota(esfera))
// 	{
// 		esfera.centro.x=0;
// 		esfera.centro.y=rand()/(float)RAND_MAX;
// 		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
// 		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
// 		puntos1++;
//     
// 	}
	
 	p_dmc->esfera = esfera; //Esfera
 	p_dmc->raqueta1 = jugador1; //Raqueta
	
  	//Actualizacion 
 	switch(p_dmc->accion){
     case 1: //Arriba
       OnKeyboardDown('w', 0, 0);
       break;
       
     case -1: //Abajo
      OnKeyboardDown('s', 0, 0);
       break;
   }
   
   //Lectura de la FIFO que viene del servidor
   char cad[200];
   int aux = read(fd_Ser2Cli, cad, 200);
   if(aux < 0) 
// perror("Error en el read del cliente");
	exit(1);
   else {
      if (aux > 0) 
         sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %f %d %d",
                &esfera.centro.x,&esfera.centro.y, &esfera.radio,
                &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2,
                &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2,
                &puntos1, &puntos2); 
   }
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
// 	switch(key)
// 	{
// 	case 's':jugador1.velocidad.y=-4;break;
// 	case 'w':jugador1.velocidad.y=4;break;
//    case 'd':
//       if(disparo1 == NULL){
//          //Crea el disparo
//          disparo1 = new Esfera;
//          disparo1->radio = 0.2f;
//          
//          //Le asigna una posicion inicial
//          disparo1->centro.x = jugador1.x2 + 0.2f;
//          disparo1->centro.y = (jugador1.y2 - jugador1.y1)/2;
//          
//          //Le asigna una velocidad
//          disparo1->velocidad.x = 6;
//          disparo1->velocidad.y = 0;
//       }
//       
//       break;
//    
// 	case 'l':jugador2.velocidad.y=-4;break;
// 	case 'o':jugador2.velocidad.y=4;break;
//    case 'k':
//       if(disparo2 == NULL){
//          //Crea el disparo
//          disparo2 = new Esfera;
//          disparo2->radio = 0.2f;
//          
//          //Le asigna una posicion inicial
//          disparo2->centro.x = jugador1.x2 - 0.2f;
//          disparo2->centro.y = (jugador1.y2 - jugador1.y1)/2;
//          
//          //Le asigna una velocidad
//          disparo2->velocidad.x = -6;
//          disparo2->velocidad.y = 0;
//       }
//       
//       break;
// 	
//       
//    }
   
   //Escribimos valor de la tecla en la tubería Cli2Ser
   char buff[100];
   sprintf(buff, "%c", key);
   write(fd_Cli2Ser, &buff, sizeof(char));
   
}

void CMundo::Init()
{
	Plano p;
   
   //Disparos
   disparo1 = NULL;
   disparo2 = NULL;
   
   //pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

  //superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

   // Creacion de area compartida para el BOT
   int fd_dmc = open("/tmp/mi_bot", O_CREAT | O_RDWR | O_TRUNC, 0777);
   if(fd_dmc < 0) exit(1);

   int wr_bot = write(fd_dmc, &dmc, sizeof(dmc));
   if(wr_bot < 0) exit(1);

   p_dmc = (DatosMemCompartida*)mmap(NULL, sizeof(dmc), PROT_READ | PROT_WRITE,
                                     MAP_SHARED, fd_dmc, 0);
   if(p_dmc < 0) exit(1);
   close(fd_dmc);
   
   //Creacion de la FIFO Ser2Cli
   int aux2 = mkfifo("/tmp/fifo_ser2cli", 0777);
   if(aux2 < 0) exit(1);
  //Creacion de la FIFO Cli2Ser
   int aux = mkfifo("/tmp/fifo_cli2ser", 0777);
   if(aux < 0) exit(1); 
   
   //Apertura de la FIFO Ser2Cli
   fd_Ser2Cli = open("/tmp/fifo_ser2cli", O_RDONLY);
   if(fd_Ser2Cli < 0) exit(1);
   //Apertura de la FIFO Cli2Ser
   fd_Cli2Ser = open("/tmp/fifo_cli2ser", O_WRONLY);
   if(fd_Cli2Ser < 0) exit(1);
   
}
