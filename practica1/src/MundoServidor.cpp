// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <cstdlib>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <iostream>
#include <unistd.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


void* hilo_comandos(void* d)
{
      CMundo* p=(CMundo*) d;
      p->RecibeComandosJugador();
}

void CMundo::RecibeComandosJugador()
{
     while (1) {
            usleep(10);
            char cad[100];
            read(fd_Cli2Ser, cad, sizeof(cad));
            unsigned char key;
            sscanf(cad,"%c",&key);
            if(key=='s')jugador1.velocidad.y=-4;
            if(key=='w')jugador1.velocidad.y=4;
            if(key=='l')jugador2.velocidad.y=-4;
            if(key=='o')jugador2.velocidad.y=4;
      }
}


CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
  write(fd_fifo, "-", strlen("-"));
  close(fd_fifo);
  close(fd_Ser2Cli);
  close(fd_Cli2Ser);

}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();
   if(disparo1 != NULL) disparo1->Dibuja();
   if(disparo2 != NULL) disparo2->Dibuja();
	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	esfera.Decrece(0.025f);
   	if(disparo1 != NULL) disparo1->Mueve(0.025f);
   
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
      paredes[i].Colisiona(disparo1);
      paredes[i].Colisiona(disparo2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
   jugador2.Colisiona(disparo1);
   jugador1.Colisiona(disparo2);
   
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
    char buffer[100];
    sprintf(buffer, "Jugador 2 marca 1 punto, lleva un total de %d puntos.\n",
            puntos2);
    write(fd_fifo, buffer, strlen(buffer));
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
    char buffer[100];
    sprintf(buffer, "Jugador 1 marca 1 punto, lleva un total de %d puntos.\n",
            puntos1);
    write(fd_fifo, buffer, strlen(buffer));
	}
	
  
   //Construimos la cadena de texto con los datos a enviar
   char cad[200];
   sprintf(cad, "%f %f %f %f %f %f %f %f %f %f %f %d %d",
           esfera.centro.x,esfera.centro.y, esfera.radio,
           jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,
           jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2,
           puntos1, puntos2);
   
   //Enviamos los datos por la tubería Ser2Cli
   write(fd_Ser2Cli, cad, 200);

}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
  case 'd':
      if(disparo1 == NULL){
         //Crea el disparo
         disparo1 = new Esfera;
         disparo1->radio = 0.2f;
         
         //Le asigna una posicion inicial
         disparo1->centro.x = jugador1.x2 + 0.2f;
         disparo1->centro.y = (jugador1.y2 - jugador1.y1)/2;
         
         //Le asigna una velocidad
         disparo1->velocidad.x = 6;
         disparo1->velocidad.y = 0;
      }
      
      break;
   
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;
  case 'k':
      if(disparo2 == NULL){
         //Crea el disparo
         disparo2 = new Esfera;
         disparo2->radio = 0.2f;
         
         //Le asigna una posicion inicial
         disparo2->centro.x = jugador1.x2 - 0.2f;
         disparo2->centro.y = (jugador1.y2 - jugador1.y1)/2;
         
         //Le asigna una velocidad
         disparo2->velocidad.x = -6;
         disparo2->velocidad.y = 0;
      }
      
      break;
	
      
   }
}

void CMundo::Init()
{
	Plano p;
   
   //Disparos
   disparo1 = NULL;
   disparo2 = NULL;
   
   //pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

  //superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
   
   //Creamos el thread
  pthread_create(&thid1, NULL, hilo_comandos, this);
  
  //Apertura de tuberia LOGGER
  fd_fifo = open(NAME_FIFO, O_WRONLY);
  
  
  //Apertura de la FIFO que debe leerse (Ser2Cli)
  fd_Ser2Cli = open("/tmp/fifo_ser2cli", O_WRONLY);  
  
  //Apertura de la FIFO que debe leerse (Cli2Ser)
  fd_Cli2Ser = open("/tmp/fifo_cli2ser", O_RDONLY);
}
