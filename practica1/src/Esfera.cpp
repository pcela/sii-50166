// Esfera.cpp: implementation of the Esfera class.
//
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=0.5f;
	velocidad.x=3;
	velocidad.y=3;
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	glColor3ub(255,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
	centro.x = centro.x + velocidad.x * t;
	centro.y = centro.y + velocidad.y * t;

   //centro.x=centro.x+velocidad.x*t+0.5*aceleracion.x*t*t;
   //centro.y=posicion.y+velocidad.y*t+0.5*aceleracion.y*t*t;
	//velocidad.x=velocidad.x+aceleracion.x*t;
	//velocidad.y=velocidad.y+aceleracion.y*t;

}

void Esfera::Decrece(float t){
	if(radio >0.01)	radio -= 0.05f*t;
	else radio = 0.5f;
}


