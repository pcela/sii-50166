// Logger.cpp: implementation of the Logger class.
//
//////////////////////////////////////////////////////////////////////

#include "Logger.h"
#include "glut.h"
#include "Common.h"
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <cstring>
#include <stdlib.h>
#include <errno.h>

using namespace std;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Logger::Logger() {
  name_fifo = NAME_FIFO;
  int aux = mkfifo(name_fifo.c_str(), 0777); 
  if(aux < 0) {
	cout << "Error en la creacion de la FIFO." << endl;	
	exit(1);
  }

  fd_fifo = open(name_fifo.c_str(), O_RDONLY|O_TRUNC);
  if(fd_fifo < 0) {
	cout << "Error en la apertura de la FIFO." << endl;	
	exit(1);
  }
}

Logger::~Logger() {
  close(fd_fifo);
  unlink(name_fifo.c_str());
//   cout << "Destruyendo logger" << endl;
}

void Logger::Read(void) {
  int aux;
  while(true) {
    aux = read(fd_fifo, buffer, MAX_BUFFER);
    if(aux < 0) {
      cout << "Error en la lectura de la FIFO. Codigo: " << strerror(errno) << endl;
//       perror(sys_errlist[errno]);
      break;
    }

//     if (strncmp(GAME_OVER, buffer, sizeof(GAME_OVER)) == 0) break;
   if (buffer[0] == '-') break;
    cout << buffer;
  };  
}


 
