// Logger.h: interface for the Logger class.
// Author: Paula Cela
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LOGGER_H__8D520BAF_8208_423B_BD91_29F6687FB9C3__INCLUDED_)
#define AFX_LOGGER_H__8D520BAF_8208_423B_BD91_29F6687FB9C3__INCLUDED_


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Vector2D.h"
#include <string>
#include <unistd.h>

#define MAX_BUFFER 100

using namespace std;

class Logger  
{
  int fd_fifo;
  string name_fifo;
  char buffer[MAX_BUFFER];
  
public: 
  Logger();
  virtual ~Logger();
  void Read(void);
};

#endif // !defined(AFX_LOGGER_H__8D520BAF_8208_423B_BD91_29F6687FB9C3__INCLUDED_)

