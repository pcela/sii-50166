// Mundo.h: interface for the CMundo class.
// Author: Paula Cela
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"
#include "Common.h"
#include "DatosMemCompartida.h"
#include <string>
#include <pthread.h>


using namespace std;

class CMundo  
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();
   void RecibeComandosJugador();
  
  string name_fifo;
  int fd_fifo;
  int fd_Cli2Ser;
   int fd_Ser2Cli;
   
   pthread_t thid1;

	Esfera esfera;
  Esfera *disparo1;
  Esfera *disparo2;
   
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
  
   DatosMemCompartida dmc;
   DatosMemCompartida *p_dmc;

	int puntos1;
	int puntos2;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
