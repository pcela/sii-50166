#pragma once 
#include "Esfera.h"
#include "Raqueta.h"

class DatosMemCompartida {       
  public:
     DatosMemCompartida();
     virtual ~DatosMemCompartida();
     
      Esfera esfera;
      Raqueta raqueta1;
      int accion; //1 arriba, 0 nada, -1 abajo
      bool salir;
}; 
